1) Criar uma rota para iniciar o jogo, resposta: "Game status: Active", status: 200

2) Criar uma rota para finalizar o jogo, resposta: "Game status: Inactive", status: 200

3) O jogo não pode ser finalizado se já estiver finalizado, nem iniciado se já estiver iniciado, resposta: "Illegal operation", status: 409