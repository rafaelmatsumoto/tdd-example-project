import app from '../app';
import request from 'supertest'

describe("Game API", () => {
    it("should start the game", async () => {
        const response = await request(app).get("/game/start");

        expect(response.status).toEqual(200);
        expect(response.body).toEqual({ message: "Game status: Active" });
    });

    it("should finish the game", async () => {
        const response = await request(app).get("/game/finish");

        expect(response.status).toEqual(200);
        expect(response.body).toEqual({ message: "Game status: Inactive" });
    });

    it("should test route constraint", async () => {
        await request(app).get("/game/start");
        const start = await request(app).get("/game/start");

        expect(start.status).toEqual(409);
        expect(start.body).toEqual({ message: "Illegal operation" });

        await request(app).get("/game/finish");
        const finish = await request(app).get("/game/finish");

        expect(finish.status).toEqual(409);
        expect(finish.body).toEqual({ message: "Illegal operation" });
    });

    it("should test the whole app", async () => {
        await request(app).get("/game/start");
        const finish = await request(app).get("/game/finish");

        expect(finish.status).toEqual(200);
        expect(finish.body).toEqual({ message: "Game status: Inactive" });
    })
})