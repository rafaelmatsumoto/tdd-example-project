import { Router, Request, Response } from "express";
const router = Router();
let gameStarted = false;

router.get("/status", async (req: Request, res: Response) => {
  return res.status(200).json({ message: "ok" });
});

router.get("/start", async (req: Request, res: Response) => {
  if (gameStarted) {
    return res.status(409).json({ message: "Illegal operation" });
  }
  gameStarted = true;
  return res.status(200).json({ message: "Game status: Active" });
})

router.get("/finish", async (req: Request, res: Response) => {
  if (!gameStarted) {
    return res.status(409).json({ message: "Illegal operation" });
  }
  gameStarted = false;
  return res.status(200).json({ message: "Game status: Inactive" });
})

export default router;
