import * as dotenv from "dotenv";
import express from "express";
dotenv.config();
import cors from "cors";
import game from "./api/routes/game";
const app = express();
app.use(cors());
app.use("/game", game);
export default app;
