# TDD Example Project

## Prerequisites

- Node: v12.19.0
- Npm: v6.14.8

## Installation

```bash
npm i
```

## Usage

```bash
npm run dev
```

## Testing

```bash
npm t
```

## License

[MIT](https://choosealicense.com/licenses/mit/)
