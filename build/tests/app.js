"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const supertest_1 = __importDefault(require("supertest"));
const app_1 = __importDefault(require("../app"));
describe("The game API", () => {
    afterEach(() => __awaiter(void 0, void 0, void 0, function* () {
        yield supertest_1.default(app_1.default).get("/game/finish");
    }));
    it("should return that the game has not started yet", () => __awaiter(void 0, void 0, void 0, function* () {
        const response = yield supertest_1.default(app_1.default).get("/game/635920/feed/live/timestamps");
        expect(response.status).toEqual(200);
        expect(response.body).toEqual({ message: "Game has not started yet" });
    }));
    it("should return the timestamps", () => __awaiter(void 0, void 0, void 0, function* () {
        yield supertest_1.default(app_1.default).get("/game/start");
        const response = yield supertest_1.default(app_1.default).get("/game/635920/feed/live/timestamps");
        expect(response.status).toEqual(200);
        expect(response.body).toContain("20201009_201052");
    }));
    it("should return diffPatch data", () => __awaiter(void 0, void 0, void 0, function* () {
        const response = yield supertest_1.default(app_1.default).get("/game/635920/feed/live/diffPatch?language=en&startTimecode=20201009_201052&endTimecode=20201009_225426");
        expect(response.status).toEqual(200);
        expect(response.body).toBeTruthy();
    }));
    it("should start the game", () => __awaiter(void 0, void 0, void 0, function* () {
        const response = yield supertest_1.default(app_1.default).get("/game/start");
        expect(response.status).toEqual(200);
        expect(response.body).toEqual({ message: "Game status: Active" });
    }));
    it("should return that the game has already started", () => __awaiter(void 0, void 0, void 0, function* () {
        yield supertest_1.default(app_1.default).get("/game/start");
        const response = yield supertest_1.default(app_1.default).get("/game/start");
        expect(response.status).toEqual(200);
        expect(response.body).toEqual({ message: "Game already started" });
    }));
    it("should finish the game", () => __awaiter(void 0, void 0, void 0, function* () {
        const response = yield supertest_1.default(app_1.default).get("/game/finish");
        expect(response.status).toEqual(200);
        expect(response.body).toEqual({ message: "Game status: Inactive" });
    }));
});
