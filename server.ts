import app from "./app";

const PORT = process.env.PORT;
app.listen(PORT, () => {
  console.log(`[server]: Server is up and running on port: ${PORT}`);
});
